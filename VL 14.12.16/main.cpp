#include <cstdlib>
#include <iostream>
// #include <string.h>
#include <stdio.h>

using namespace std;

void swap (int *x, int *y){
    double tmp;
    
    tmp = *x;
    *x = *y;
    *y = tmp;
    
}

int strlen(char *text){
    
    char *ptr;
    int c = 0;
    
    ptr = text;
    
    while (*ptr != '\0')
    {
        c++;
        ptr++;
    }
    
    return c;
    
}



int main(int argc, char** argv) {

    int a = 3;
    int b = 5;
    int l = 0;
    char buffer[] = "Das ist die laenge";
    
    cout << "a=" << a << "\n";
    cout << "b=" << b << "\n\n";
    
    cout << "nach Wechsel" << "\n\n";
    
    swap(&a, &b);
    
    cout << "a=" << a << "\n";
    cout << "b=" << b << "\n\n";
    
    l=strlen(buffer);
    
    cout << l << "\n\n"; 
    
    return 0;
}

