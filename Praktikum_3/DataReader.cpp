#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <string.h>
#include "DataReader.h"
//#include "Corrector.h";
//#include "CorrectorLinear.h";


using namespace std;



DataReader::DataReader(){
    
}

DataReader::~DataReader(){
    if (Daten != NULL)
    {
         delete Daten;
    }
}

void DataReader::readDataFrom(const char *FileName){
  
    double *ptr;
    int i=0;
   
    char buffer[]="             ";
    FILE *datafile;
    
    
    if (Daten != NULL)
    {
    Daten = new double[200]; 
    ptr = Daten;
    
    
    datafile=fopen(FileName, "rt");
    while (i<200){
    fgets(buffer,20,datafile);   
    *ptr = atof (buffer);
    //printf("%f\n",*ptr);
    ptr++;
    i++;
     }   
    }
    
    
    
}

void DataReader::setDataCorrection(Corrector *corrector){
    double *ptr;
    double *ptr_out;
    Daten_out = new double[200];
    ptr_out = Daten_out;
    ptr = Daten;
    int i=0;
    double k;
    while (i<200){
        k=*ptr;
        *ptr_out = corrector->correctValue(*ptr);
        cout << *ptr << "\t" << *ptr_out << "\n";  
        ptr++;
        ptr_out++;
        i++;
        
    }
    
    cout << "\n\n";
}

void DataReader::writeCorrectedDataToFile(const char *file_out) {
 
    int i=0;
    double *ptr;
    char buffer[] = "                        "; 
    char line_end[] = {'\n', '\0'};
    ptr = Daten_out;
    
    
    FILE *datafile;
    datafile = fopen(file_out, "wt");

    while (i<200){
        sprintf(buffer, "%f", *ptr);
        strcat(buffer,line_end);
        fputs(buffer,datafile);
    ptr++;
    i++;
        
    }
    
    fclose(datafile);
    
}