#include "CorrectorLookupTable.h"
#include <iostream>
#include <stdio.h>

using namespace std;

CorrectorLookupTable::CorrectorLookupTable(double *x, double *y){
    
    this->x=x;
    this->y=y;
 
    sortTable();
    
}

CorrectorLookupTable::~CorrectorLookupTable(){
    ;
}

void CorrectorLookupTable::sortTable(){
    
    for (int i=0 ; i<4 ; i++){
        for (int j=i+1 ; j<5 ; j++){
        if (x[i] > x[j]){
            swap(&x[i],&x[j]);
            swap(&y[i],&y[j]);
            }
        }
    }
    
    printf("Lookup Tabelle\n");
    
    for (int i=0; i<5; i++)
    {
        cout << x[i] << "\t";
        cout << y[i] << "\n";
    }
    
    cout << "\n";
}

void CorrectorLookupTable::swap(double *x, double *y){
    double tmp;
    tmp=*y;
    *y=*x;
    *x=tmp;
    }

double CorrectorLookupTable::correctValue(double value){

    if (value < x[0])
    {
        return calculate(value,x[0],y[0],x[1],y[1]);
    }
    
    if (value >= x[0] && value < x[1])
    {
        return calculate(value,x[0],y[0],x[1],y[1]);
    }   
    
    if (value >= x[1] && value < x[2])
    {
        return calculate(value,x[1],y[1],x[2],y[2]);
    }
    
    if (value >= x[2] && value < x[3])
    {
        return calculate(value,x[2],y[2],x[3],y[3]);
    }
    
    if (value >= x[3] && value < x[4])
    {
        return calculate(value,x[3],y[3],x[4],y[4]);
    }
    
    if (value >= x[4])
    {
        return calculate(value,x[3],y[3],x[4],y[4]);
    }
}

double CorrectorLookupTable::calculate(double value, double x1, double y1, double x2, double y2){
    
    return y1+((y2-y1)/(x2-x1))*(value-x1);
    
}