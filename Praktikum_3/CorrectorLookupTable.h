#ifndef CORRECTORLOOKUPTABLE_H
#define	CORRECTORLOOKUPTABLE_H

#include "CorrectorLinear.h"

class CorrectorLookupTable : public Corrector{
    private:
        double *x;
        double *y;
        
        double calculate(double value, double x1, double y1, double x2, double y2);
        void swap(double *x, double *y);
        void sortTable();
        public:
            CorrectorLookupTable(double *x, double *y);
            ~CorrectorLookupTable();
            double correctValue(double value);
    
    
    
};


#endif	/* CORRECTORLOOKUPTABLE_H */

