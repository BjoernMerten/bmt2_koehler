#ifndef CORRECTORLINEAR_H
#define	CORRECTORLINEAR_H

#include "Corrector.h"

class CorrectorLinear : public Corrector{
    private:
        double a;
        double b;
    
    
    public:
        CorrectorLinear(double a, double b);
        ~CorrectorLinear();
        double correctValue(double Value);
        
};




#endif	/* CORRECTORLINEAR_H */

