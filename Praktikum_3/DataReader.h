#ifndef DATAREADER_H
#define	DATAREADER_H

#include "Corrector.h"


class DataReader{
private:
    double *Daten;
    double *Daten_out;
 public:   
    DataReader();
    ~DataReader();
    void readDataFrom(const char *);
    void setDataCorrection(Corrector *);
    void writeCorrectedDataToFile(const char *) ;
};



#endif	/* DATAREADER_H */

