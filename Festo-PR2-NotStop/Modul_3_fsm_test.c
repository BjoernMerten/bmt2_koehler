/*

#include "cbw.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "Modul_1_Basic.h"

int a = 1;
int not = 123;

int notPressed() {
    updateProcessImage();
    if ((sens_change(e_Stop))== 1) return (0);
    return (12);
}

void FSM1start() {
    resetOutputs();
    setBitInOutput(LED_Start);
    applyOutputToProcess();
    updateProcessImage();
    if ((sens_change(key_Start))== 1) a = 2;
}

void FSM2work() {
    resetOutputs();
    setBitInOutput(light_Green);
    applyOutputToProcess();
    updateProcessImage();
    if ((sens_change(key_Start))== 1) a = 1;
    else if ((sens_change(entry))== 2) a = 3;
}

void FSM3transportToHigh() {
    setBitInOutput(engine_Right | light_Yellow);
    clearBitInOutput(light_Green);
    applyOutputToProcess();
    updateProcessImage();
    if ((sens_change(inHeightcheck))== 2) a = 4;
}

void FSM4high() {
    setBitInOutput(engine_Stop | light_Red);
    clearBitInOutput(engine_Right | light_Yellow);
    applyOutputToProcess();
    updateProcessImage();
    if (isBitSet(heigh_Ok) == 1) a = 5;
    else a = 6;
}

void FSM5transportFromHigh1() {
    setBitInOutput(engine_Slow | light_Green | LED_Q1);
    clearBitInOutput(light_Red | engine_Stop);
    applyOutputToProcess();
    printf("\nHoehenmessung: OK\n\n");
    updateProcessImage();
    if ((sens_change(piece_In_Switch))== 2) a = 7;
}

void FSM6transportFromHigh2() {
    setBitInOutput(light_Red);
    clearBitInOutput(light_Yellow);
    applyOutputToProcess();
    printf("\nHoehenmessung: NICHT OK\n\n");
    updateProcessImage();
    if ((sens_change(piece_In_Switch))== 2) a = 7;
}

void FSM7metal() {
    setBitInOutput(light_Red | engine_Stop);
    clearBitInOutput(light_Green | light_Yellow | engine_Slow);
    applyOutputToProcess();
    updateProcessImage();
    if (isBitSet(metal_Ok) == 1) a = 9;
    else a = 8;
}

void FSM8switchOpen() {
    setBitInOutput(engine_Right | light_Yellow | switch_Off | LED_Q2);
    clearBitInOutput(light_Red | engine_Stop);
    applyOutputToProcess();
    updateProcessImage();
    if ((sens_change(exit))== 2) a = 2;
}

void FSM9switchClose() {
    setBitInOutput(engine_Right | light_Yellow | light_Red);
    clearBitInOutput(engine_Stop);
    applyOutputToProcess();
    updateProcessImage();
    if ((sens_change(store_Full))== 2) a = 10;
}

void FSM10store() {
    setBitInOutput(engine_Stop);
    clearBitInOutput(engine_Right | light_Yellow);
    applyOutputToProcess();
    for (int i = 0; i < 100000000; i++);
    a = 2;
}

void FSM() {
    not = notPressed();
    while (not != 0) {
        switch (a) {
            case(1):
                FSM1start();
                break;
            case(2):
                FSM2work();
                break;
            case(3):
                FSM3transportToHigh();
                break;
            case(4):
                FSM4high();
                break;
            case(5):
                FSM5transportFromHigh1();
                break;
            case(6):
                FSM6transportFromHigh2();
                break;
            case(7):
                FSM7metal();
                break;
            case(8):
                FSM8switchOpen();
                break;
            case(9):
                FSM9switchClose();
                break;
            case(10):
                FSM10store();
                break;
        }
    }
    resetOutputs();
    applyOutputToProcess();
    printf("Notstop gedrückt, bitte Anlage neustarten.");
}
*/