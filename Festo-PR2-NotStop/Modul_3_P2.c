#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "cbw.h"
#include "Modul_1_Basic.h"

int state = 1;
int high1 = 1, high2 = 1, metall1 = 1, metall2 = 1, weiche = 1, emergency = 1;

/*int notPressed() {
    updateProcessImage();
    if (Sensors0 == e_Stop) return (0);
    return (12);
}*/

void FSM() {

    while (emergency == 1) {
        updateProcessImage();
        
        if (isBitSet(e_Stop) == 0) {
            resetOutputs();
            emergency = 0;
            setBitInOutput(light_Red);
            Sleep(1000);
            printf("Irgendein Mist\n");
        }
        
        switch (state) {
            case(1):
                FSM1start();
                break;
            case(2):
                FSM2work();
                break;
            case(3):
                FSM3transportToHeighcheck();
                break;
            case(4):
                FSM4highcheck();
                break;
            case(41):
                FSM41storeFull();
                break;
            case(5):
                FSM5transportToMagCheckPH();
                break;
            case(6):
                FSM6transportToMagCheckNH();
                break;
            case(7):
                FSM7magCheck();
                break;
            case(8):
                FSM8sortNM();
                break;
            case(9):
                FSM9sortPM();
                break;
            case(10):
                FSM10store();
                break;
            case(11):
                FSM11exit();
                break;
        }
        applyOutputToProcess();
        emergency = 1;
    }

    

}

void FSM1start() {
    resetOutputs();
    setBitInOutput(LED_Start);
    if (sens_change(key_Start) == 1) state = 2;
}

void FSM2work() {
    setBitInOutput(light_Green);
    if (sens_change(key_Start) == 1) state = 1;
    else if (sens_change(entry) == 2) state = 3;
}

void FSM3transportToHeighcheck() {
    clearBitInOutput(light_Green | LED_Start | light_Red);
    setBitInOutput(engine_Right | light_Yellow);
    if (sens_change(inHeightcheck) == 2) state = 4;
}

void FSM4highcheck() {
    clearBitInOutput(engine_Right | light_Yellow);
    setBitInOutput(light_Red);
    if (isBitSet(store_Full) == 0)
        state = 41;
    else if (isBitSet(heigh_Ok) == 1) state = 6;
    else state = 5;
    weiche = 1;
}

void FSM41storeFull() {
    if (weiche == 1) {
        printf("Bitte die Rutsche entleeren, da diese VOLL ist\ndanach \"Reset\" pressen\n");
        weiche = 0;
    }
    if (sens_change(key_Reset) == 1)state = 4;
}

void FSM5transportToMagCheckPH() {
    clearBitInOutput(light_Red);
    setBitInOutput(engine_Right | engine_Slow | light_Green | LED_Q1);
    if (high1 == 1) {
        printf("\nHoehenmessung: OK\n");
        high1 = 0;
    }
    if (sens_change(piece_In_Switch) == 2) state = 7;

}

void FSM6transportToMagCheckNH() {
    setBitInOutput(engine_Right | engine_Slow);
    if (high2 == 1) {
        printf("\nHoehenmessung: NICHT OK\n");
        high2 = 0;
    }
    if (sens_change(piece_In_Switch) == 2) state = 7;
}

void FSM7magCheck() {
    clearBitInOutput(engine_Right | engine_Slow | light_Green | light_Red);
    setBitInOutput(light_Red);
    high2 = 1;
    high1 = 1;
    if (isBitSet(metal_Ok) == 1) state = 9;
    else state = 8;
}

void FSM8sortNM() {
    clearBitInOutput(light_Red);
    setBitInOutput(engine_Right | light_Yellow | switch_Off | LED_Q2);
    if (metall2 == 1) {
        printf("Werkstueck nicht aus Metall\n");
        metall2 = 0;
    }
    if (sens_change(exit) == 2) state = 11;
}

void FSM9sortPM() {
    setBitInOutput(engine_Right | light_Yellow);
    if (metall1 == 1) {
        printf("Werkstueck aus Metall\n");
        metall1 = 0;
    }
    if (sens_change(store_Full) == 2) state = 10;
}

void FSM10store() {
    clearBitInOutput(engine_Right | light_Yellow);
    metall1 = 1;
    if (sens_change(entry) == 2) state = 3;
    clearBitInOutput(LED_Q1);
    if (sens_change(key_Stop) == 2) state = 1;
}

void FSM11exit() {
    clearBitInOutput(engine_Right | switch_Off | LED_Q2 | light_Yellow);
    setBitInOutput(light_Green);
    metall2 = 1;
    if (sens_change(entry) == 2) {
        state = 3;
        clearBitInOutput(LED_Q1);
    }
    if (sens_change(key_Stop) == 2) state = 1;
}


