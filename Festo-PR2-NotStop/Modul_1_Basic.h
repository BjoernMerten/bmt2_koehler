/* 
 * File:   Modul_Basic.h
 * Author: Björn Merten & Daniel Friedrich
 *
 * Created on 31. März 2014, 16:35
 */

#ifndef MODUL_1_BASIC_H
#define	MODUL_1_BASIC_H

#define engine_Right 0x0001
#define engine_Left 0x0002
#define engine_Slow 0x0004
#define engine_Stop 0x0008
#define switch_Off 0x0010
#define light_Green 0x0020
#define light_Yellow 0x0040
#define light_Red 0x0080

#define entry 0x0001
#define inHeightcheck 0x0002
#define heigh_Ok 0x0004
#define piece_In_Switch 0x0008
#define metal_Ok 0x0010
#define switch_Open 0x0020
#define store_Full 0x0040
#define exit 0x0080

#define LED_Start 0x0100
#define LED_Reset 0x0200
#define LED_Q1 0x0400
#define LED_Q2 0x0800

#define key_Start 0x0100
#define key_Stop 0x0200
#define key_Reset 0x0400
#define e_Stop 0x0800

#define MAXX 16


#ifdef	__cplusplus
extern "C" {
#endif

     

// basic funcions - for all tasks - File: Modul_1_Basic.c
     
     
    void Initialisierung();
    void updateProcessImage();
    void applyOutputToProcess();
    unsigned int isBitSet(unsigned short ChMask);
    void setBitInOutput(unsigned short SetMask);
    void clearBitInOutput(unsigned short ClearMask);
    void resetOutputs();
    
    int sens_change(unsigned short Mask);
    
// functions for practical training 1 - File: Modul_2_P1    
    
    void Testrun();
    void Sensorstatus();
    void stop();
   
    void entryPrint(int c);
    void inHeightcheckPrint(int c);
    void heigh_OkPrint(int c);
    void piece_In_SwitchPrint(int c);
    void metal_OkPrint(int c);
    void switch_OpenPrint(int c);
    void store_FullPrint(int c);
    void exitPrint(int c);
    void key_StartPrint(int c);
    void key_StopPrint(int c);
    void key_ResetPrint(int c);
    void e_StopPrint(int c);
    
    
// functions for practical training 2 - File: Modul_3_P2.c

    void FSM();
    void FSM1start();
    void FSM2work();
    void FSM3transportToHeighcheck();
    void FSM4highcheck();
    void FSM5transportToMagCheckPH();
    void FSM6transportToMagCheckNH();
    void FSM7magCheck();
    void FSM8sortNM();
    void FSM9sortPM();
    void FSM10store();
    void FSM11exit();
    void FSM41storeFull();
    
    
        
    

#ifdef	__cplusplus
}
#endif

#endif	/* MODUL_BASIC_H */