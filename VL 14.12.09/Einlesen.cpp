#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include "Einlesen.h"

int Einlesen::get_anzahl(){
    return anzahl;
}

Einlesen::~Einlesen(){
    if(Daten != NULL)
        delete Daten;
}

Einlesen::Einlesen(int anzahl){
    this->anzahl=anzahl;
    }

void Einlesen::macheArray(){
    double *ptr;
    Daten = new double[anzahl];
    ptr=Daten;
    int i=0;
    
    while (i<anzahl){
        double x = (double) rand()/10000*5;
        *ptr=x;
        ptr++;
        i++;}
}

void Einlesen::zeigeArray(){
    double *ptr;
    ptr=Daten;
    int i=0;
    while (i<anzahl){
        
        printf("Adresse von = %X\t Wert = %f\n",ptr,*ptr);
        ptr++;
        i++;}
    
    
}



