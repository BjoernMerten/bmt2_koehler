#include <cstdlib>
#include <time.h>
#include <iostream>

#include "Einlesen.h"

using namespace std;

int main(int argc, char** argv) {

    int anz=10;
    
    Einlesen A(anz);
    
    //cout << A.get_anzahl() << endl;
    
    A.macheArray();
    A.zeigeArray();
    
    //srand((unsigned)time(NULL));
    
    //=rand()%10;
    
    return 0;
}

