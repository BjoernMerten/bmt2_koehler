#ifndef EINLESEN_H
#define	EINLESEN_H

class Einlesen{
private:
    double *Daten;
    int anzahl;
public:
    Einlesen(int anzahl);
    ~Einlesen();
    void macheArray();
    void zeigeArray();
    int get_anzahl();
    
};
#endif	/* EINLESEN_H */

