#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

    int i = 22;
    double e = 2.71828;
    int Pr[6] = {2, 3, 5, 7, 11, 13};
    int anzahl = sizeof (Pr) / sizeof (int);

    FILE *out = NULL;

    char OutputFileName[] = "C:\\beans_c\\Beispiel.dat";
    out = fopen(OutputFileName, "wb");

    if (out == NULL) {
        printf("Datei %s konnte nicht geschrieben werden \n", OutputFileName);
        return -1;
    }

    //Verarbeitung

    fwrite(&i, sizeof (int), 1, out);
    fwrite(&e, sizeof (double), 1, out);
    fwrite(Pr, sizeof (int), anzahl, out);

    //Schließen der Datei

    if (out)
        fclose(out);



    return (EXIT_SUCCESS);
}

