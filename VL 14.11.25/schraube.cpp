#include <cstdlib>
#include <iostream>
#include <string>
#include "schraube.h"

using namespace std;

bool schraube::initialisieren(unsigned int _Laenge, double _Durchmesser, const string& _Farbe){
    
    if(_Farbe.size() > 2){
        
        laenge = _Laenge;
        durchmesser = _Durchmesser;
        farbe = _Farbe;
    }
    else {
        return false;
    }
    return true;
}

void schraube::SchraubenDatenAnzeige(){
    
    cout <<"\nSchraubenlaenge = " << laenge
            <<"\nSchraubendurchmesser = " << durchmesser
            <<"\nSchraubenfarbe = " << farbe <<"\n\n";
    
    
};

/*schraube::schraube(unsigned int _Laenge, double _Durchmesser, const string& _Farbe){
    laenge = _Laenge;
        durchmesser = _Durchmesser;
        farbe = _Farbe;
};
*/

bool schraube::SetFarbe(const string& NeueFarbe){
    
    string Farben[3]=("Schwarz","Kupfer","Silber");
    for (int i=0; i < 3; i++)
        if (Farben[i]==NeueFarbe){
         farbe=NeueFarbe;
         return true;    
        }
    return false;
}

const string schraube::getFarbe(){
    return farbe;
    }
