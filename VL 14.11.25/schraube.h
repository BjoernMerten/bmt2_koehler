
#ifndef SCHRAUBE_H
#define	SCHRAUBE_H

#include <string>

using namespace std;

class schraube {
public:
    schraube(unsigned int, double, const string&);
    bool initialisieren (unsigned int, double, const string&);
    void SchraubenDatenAnzeige();
    bool SetFarbe (const string &NeueFarbe);
    const string getFarbe();
private:
    unsigned int laenge;
    double durchmesser;
    string farbe;
    string NeueFarbe;
};

#endif	/* SCHRAUBE_H */

