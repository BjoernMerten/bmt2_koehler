#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define EINGABELAENGE 20

char *filename="bank.csv";

void clearInput(void);
void addNewAccount(void);
void listAll(void);
void deleteAccount (void);
void cleanUp(void);
void saveAccounts(void);
void loadAccounts(void);


struct account
{
   int number;
   char lastname[EINGABELAENGE+1];
   char firstname[EINGABELAENGE+1];
   float balance;
   struct account *next;
};

struct account *firsta;
int anum=0;

int main() 
{
    char ch;
    firsta=NULL;
    
    loadAccounts();
    
    do
    {
        clearInput();
        printf("\n\tN - Neues Konto");
        printf("\n\tL - Konto loeschen");
        printf("\n\tA - Alle Konten anzeigen");
        printf("\n\tE - Ende\n");
        printf("\n\n\tIhre Wahl: ");
        ch=getchar();
        ch=toupper(ch);
    
    
    switch(ch)
    {
        case 'N':
            printf("\n\tNeues Konto anlegen\n\n");
            clearInput();
            addNewAccount();
            break;
        case 'L':
            printf("\n\tKonto loeschen\n\n");
            clearInput();
            deleteAccount();
            break;
        case 'A':
            printf("\n\tAlle Konten anzeigen\n\n");
            clearInput();
            listAll();
            break;
        case'E':
            printf("\n\n\tProgrammende\n\n\n\t");
            saveAccounts();
            cleanUp();
            break;
        default:
            break;
                    
    }
    }while(ch!='E');
    
    return (0);
}

void clearInput(void)
{
    fflush(stdin);
    
}

char *entferneNL(char *string)
{
    char *string2=string;
    while (*string2)
    {
        if (*string2 == '\n')
            *string2 = '\0';
        string2++;
    }
    return string;
}

void listAll(void)
{
    struct account *currenta;
    
    if (firsta==NULL)
        puts("Keine Satensaetze");
    else
    {
        printf("%6s %-15s %-15s %-11s\n", "Konto", "Nachname", "Vorname", "Saldo");
        currenta=firsta;
        do
        { 
            printf("%5i: %-15s %-15s EUR%8.2f\n",
                    currenta->number,
                    currenta->lastname,
                    currenta->firstname,
                    currenta->balance);
        }
        while ((currenta=currenta->next) != NULL);
    }
}

void addNewAccount(void)
{
    struct account *currenta;
    struct account *newa;
    
    newa=malloc(sizeof(struct account));

/* Ist das der erste Datensatz?
   Falls ja, dann alle Zeiger daruaf einrichten
 */

if (firsta==NULL)
    currenta=firsta=newa;

/* Andernfalls das Ende der Liste finden
   und die neue Struktur am Ende Enfügen
 */
    
else
{
    currenta=firsta;
 
    while (currenta->next != NULL)
        currenta=currenta->next; /*der letzte Datensatz wurde gefunden*/
    
    currenta->next=newa; /*die Adresse des neuen speichern*/
    currenta=newa;
}

    /* Jetzt die Struktur füllen */
    
    anum++;
    printf("%27s: %5i\n", "Kontonummer", anum);
    currenta->number=anum;
    
    printf("%27s: ", "Nachname des Kunden");
    fgets(currenta->lastname, EINGABELAENGE, stdin);
    entferneNL(currenta->lastname);
    
    printf("%27s: ", "Vorname des Kunden");
    fgets(currenta->firstname, EINGABELAENGE, stdin);
    entferneNL(currenta->firstname);
    
    printf("%27s: EUR", "Kontostand");
    scanf("%f", &currenta->balance);
    
    /*
     der neue Datensatz ist das letzte Element
     der Liste, also auch kennzeichnen
     */
    
    currenta->next=NULL;
}

void deleteAccount(void)
{
    int record;
    struct account *previousa;
    struct account *currenta;
    
    if (firsta==NULL)
    {
        printf("Keine Datensaetze in der Datenbank!");
        return;
    }
    
    listAll();
    printf("Zu loeschende Kontonummer eingeben: ");
    scanf("%d", &record);
    
    currenta=firsta;
    while(currenta!=NULL)
    {
        if (currenta->number==record)
        {
            if (currenta == firsta) /* Eine Besonderheit */
                firsta=currenta->next;
            else
                previousa->next=currenta->next;
            free(currenta);
            printf("Konto %d geloescht!\n", record);
            return;
        }
        else
        {
            previousa=currenta;
            currenta=currenta->next;
        }
    }
    printf("Konto %d wurde nicht gefunden, keine Loeschung.\n", record);
}

void cleanUp()
{
    struct account *currenta;
    struct account *removea;
    
    currenta=firsta;
    
    while(currenta)
    {
        removea=currenta;
        currenta=currenta->next;
        free(removea);
    }
}

void saveAccounts()
{
    FILE *datafile;
    struct account *currenta=firsta;
    
    if (currenta==NULL)
        return;
    
    datafile=fopen(filename, "w");
    
    if (datafile==NULL)
    {
        printf("Schreibfehler in Datei %s\n", "bank.csv");
        return;
    }
    
    while (currenta != NULL)
    {
        fwrite(currenta, sizeof(struct account), 1, datafile);
        currenta=currenta->next;
    }
    fclose(datafile);
}

void loadAccounts()
{
    FILE *datafile;
    struct account *newa;
    struct account *currenta;
    firsta=NULL;
    
    datafile=fopen(filename, "r");
    if (datafile)
    {
        firsta=malloc(sizeof(struct account));
        currenta=firsta;
        while(1)
        {
            newa=malloc(sizeof(struct account));
            fread(currenta, sizeof(struct account), 1, datafile);
            if (currenta->next==NULL)
                break;
            currenta->next=newa;
            currenta=newa;
        }
        fclose(datafile);
        anum=currenta->number;
    }
}