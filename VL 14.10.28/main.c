/* 
 * File:   main.c
 * Author: joachim
 *
 * Created on 
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

/*
 * 
 */
struct s_Kundenkarte {
    char VName[50 + 1];
    char NName[50 + 1];
    char Geburtstag[10 + 1];
    int Alter;
    struct s_Kundenkarte *next;
};
struct s_Kundenkarte *first;

void anhaengen(char VName[], char NName[], char Geburtstag[], int Alter);
void ausgabe();
void loeschenmitsuchen(char NName[]);
void loeschenkomplett();
void Ausgabe_CSV();

void ausgabe() {
    struct s_Kundenkarte *ptr;
    ptr = first;
    while (ptr != NULL) {
        printf(" %s %s %s %d\n", ptr->VName, ptr->NName, ptr->Geburtstag, ptr->Alter);
        ptr = ptr->next;
    }
    printf("\n");
}

void anhaengen(char VName[], char NName[], char Geburtstag[], int Alter) {
    struct s_Kundenkarte * ptr;
    if (first == NULL) {
        first = malloc(sizeof (*first));
        strcpy((*first).VName, VName);
        strcpy(first->NName, NName);
        strcpy(first->Geburtstag, Geburtstag);
        first->Alter = Alter;
        first->next = NULL;
    } else {
        ptr = first;
        while (ptr->next != NULL)
            ptr = ptr->next; // hier wird das letzte Element gesucht!!!
        ptr->next = malloc(sizeof (*ptr));
        ptr = ptr->next; // next zeiger auf ptr ablegen um struktur zu füllen!
        strcpy((*ptr).VName, VName);
        strcpy(ptr->NName, NName);
        strcpy(ptr->Geburtstag, Geburtstag);
        ptr->Alter = Alter;
        ptr->next = NULL;
    }

}

void loeschenmitsuchen(char NName[]) {
    struct s_Kundenkarte *ptr, *ptrA;
    if (first != NULL) {
        if (strcmp(first->NName, NName) == 0) {
            ptr = first->next;
            free(first);
            first = ptr;
        } else {
            ptr = first;
            while (ptr->next != NULL) {
                ptrA = ptr->next;
                if (strcmp(ptrA->NName, NName) == 0) {
                    ptr->next = ptrA->next;
                    free(ptrA);
                    break;
                }
                ptr = ptrA;
            }
        }

    }
}

void loeschenkomplett() {

    struct s_Kundenkarte *current;
    struct s_Kundenkarte *remove;

    current = first;

    while (current != NULL) {

        remove = current->next;
        current->next = remove->next;
        free(remove);


    }


}

int main(int argc, char** argv) {
    char VName[] = "Joachim", NName[] = "Koehler", Geburtstag[] = "02.11.1953";
    int Alter = 60;
    anhaengen(VName, NName, Geburtstag, Alter);
    anhaengen("Hein", "Mueller", "15.09.1986", 28);
    anhaengen("Max", "Mutzke", "15.09.1987", 27);
    loeschenmitsuchen("Mutzke");
    ausgabe();
    Ausgabe_CSV();

    return (EXIT_SUCCESS);
}

void Ausgabe_CSV() {
    struct s_Kundenkarte *ptr;
    char OutputFileName[] = "c:\\personal.csv";
    FILE *csv_out;
    char Buffer[255];
    char delimeter[] = ";";
    char line_end[] = {'\n', '\0'};
    char alter_buffer[] = "                        ";
    ptr = first;
    Buffer[0] = '\0';
    csv_out = fopen(OutputFileName, "wt");
    while (ptr != NULL) {
        strcat(Buffer, ptr->VName);
        strcat(Buffer, delimeter);
        strcat(Buffer, ptr->NName);
        strcat(Buffer, delimeter);
        strcat(Buffer, ptr->Geburtstag);
        strcat(Buffer, delimeter);
        int k = sprintf(alter_buffer, "%i", ptr->Alter);
        strcat(Buffer, alter_buffer);
        strcat(Buffer, line_end);
        fputs(Buffer, csv_out);
        Buffer[0] = '\0';
        ptr = ptr->next;



    }
}