#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char** argv) {

    char InputFileName[] = "c:\\beans_c\\Beispiel.txt";
    char OutputFileName[] ="c:\\beans_c\\Beispiel.bak";
    
    char ch;
    
    FILE *inp; // Zeiger auf Eingabe Datei (Struktur)
    FILE *out; // Zeiger auf Ausgabe Datei (Struktur)
    
    // Öffnen der Datei(en)
    
    inp=fopen(InputFileName, "rt");
    if(inp == NULL){
        printf("Datei %s konnte nicht gelesen werden \n",InputFileName);
        return -1;
    }
    
    out=fopen(OutputFileName, "wt");
    if (out == NULL){
        printf("Datei %s konnte nicht geschieben werden \n",OutputFileName);
        fclose(inp);
        return -2;  
    }
    
    //Dateiverarbeitung
    
    while(!feof(inp)) {
        ch = fgetc(inp);
        fputc(ch,out);
    }
    
    //Datei schließen
    
    fclose(inp);
    fclose(out);
    
    
    return (EXIT_SUCCESS);
}

