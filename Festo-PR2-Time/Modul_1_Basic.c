#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include "cbw.h"
#include "Modul_1_Basic.h"


unsigned short Sensors = 0x0000;
unsigned short Actors = 0x0000;
unsigned short Sensors0 = 0x0000;
unsigned short Sensors1 = 0x0000;
unsigned short SensorsOld = 0x0000;

void Initialisierung() {
    cbDConfigPort(0, FIRSTPORTA, DIGITALOUT);
    cbDConfigPort(0, FIRSTPORTB, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCH, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCL, DIGITALOUT);
}

void updateProcessImage() {
    unsigned short SensorsFlush;
    SensorsOld = Sensors;
    cbDIn(0, FIRSTPORTCH, &Sensors);
    Sensors <<= 8;
    cbDIn(0, FIRSTPORTB, &SensorsFlush);
    Sensors = SensorsFlush | Sensors;
    Sensors1 = (SensorsOld ^ Sensors) & Sensors;
    Sensors0 = (SensorsOld ^ Sensors) & SensorsOld;
}

void applyOutputToProcess() {
    unsigned short out;
    out = Actors;
    cbDOut(0, FIRSTPORTA, out);
    out >>= 8;
    cbDOut(0, FIRSTPORTCL, out);
}

unsigned int isBitSet(unsigned short ChMask) {
    if ((Sensors & ChMask) == ChMask)
         return 1;
    else return 0;
}

void setBitInOutput(unsigned short SetMask) {
    Actors |= SetMask;
}

void clearBitInOutput(unsigned short ClearMask) {
    Actors &= (~ClearMask);
}

void resetOutputs() {
    Actors = 0x0000;
    applyOutputToProcess();
}

int sens_change(unsigned short Mask) {
    if ((Sensors1 & Mask) != 0)
        return 1;
    if ((Sensors0 & Mask) != 0)
        return 2;
    return 0;
}
