#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <string.h>
#include "cbw.h"
#include "Modul_1_Basic.h"
#include "Modul_2_P1.h"
#include "Modul_3_P2.h"

int state = 1;
    

void FSM() {
    
    while (1) {
        updateProcessImage();
        switch (state) {
            case(1):
                FSM1start();
                break;
            case(2):
                FSM2work();
                break;
            case(3):
                FSM3transportToHeighcheck();
                break;
            case(4):
                FSM4highcheck();
                break;
            case(5):
                FSM5transportToMagCheckPH();
                break;
            case(6):
                FSM6transportToMagCheckNH();
                break;
            case(7):
                FSM7magCheck();
                break;
            case(8):
                FSM8sortNM();
                break;
            case(9):
                FSM9sortPM();
                break;
            case(10):
                FSM10store();
                break;
            case(11):
                FSM11exit();
                break;
            case(12):
                break;
                
        }
        applyOutputToProcess();
        if (state == 12) 
        {
            state = 1;
            resetOutputs();
            break;
        }
    }
    
}

void FSM1start() {
    clearBitInOutput(light_Green);
    setBitInOutput(LED_Start);
    if (sens_change(key_Start) == 1) state = 2;
    else if (sens_change(key_Stop) == 2) state = 12;
}

void FSM2work() {
    clearBitInOutput(key_Start);
    setBitInOutput(light_Green);
    if (sens_change(key_Start) == 1) 
    {
        state = 1;
    }
    else if (sens_change(entry) == 2) 
    {
        state = 3;
        InputTime();
    }
}
    
void FSM3transportToHeighcheck() {
    clearBitInOutput(light_Green | LED_Start | light_Red);
    setBitInOutput(engine_Right | light_Yellow);
    if (sens_change(inHeightcheck) == 2) state = 4;
}

void FSM4highcheck() {
    clearBitInOutput(engine_Right | light_Yellow);
    setBitInOutput(light_Red);
    applyOutputToProcess();
    Sleep(1000);
    updateProcessImage();
    if (isBitSet(heigh_Ok) == 1) 
    {
        state = 6;
        strcpy(daten.height,"Hoehe OK");
        printf("Hoehe OK\n");
    }
    else 
    {
        state = 5;
        strcpy(daten.height,"Hoehe Falsch");
        printf("Hoehe nicht OK\n");
    }
}
    
void FSM5transportToMagCheckPH() {
    clearBitInOutput(light_Red);
    setBitInOutput(engine_Right | engine_Slow | light_Green | LED_Q1);
    if (sens_change(piece_In_Switch) == 2) state = 7;
}

void FSM6transportToMagCheckNH() {
    setBitInOutput(engine_Right | engine_Slow);
    if (sens_change(piece_In_Switch)== 2) state = 7;
}

void FSM7magCheck() {
    clearBitInOutput(engine_Right | engine_Slow | light_Green | light_Red);
    setBitInOutput(light_Red);
    applyOutputToProcess();
    Sleep(1000);
    updateProcessImage();
    if (isBitSet(metal_Ok) == 1)
    {
        state = 9;
        strcpy(daten.metal,"Werkstueck aus Metall");
        printf("Werkstueck aus Metall\n");
    }
    else 
    {
        state = 8;
        strcpy(daten.metal,"Werkstueck nicht aus Metall");
        printf("Werkstueck nicht aus Metall\n");
    }
}

void FSM8sortNM() {
    clearBitInOutput(light_Red);
    setBitInOutput(engine_Right | light_Yellow | switch_Off | LED_Q2);
    if (sens_change(exit) == 2) 
        {
        state = 11;
        OutputTime();
        writeList();
        }
}
    
void FSM9sortPM() {
    clearBitInOutput(light_Red);
    setBitInOutput(engine_Right | light_Yellow);
    if (sens_change(store_Full) == 2) 
        {
        state = 10;
        OutputTime();
        writeList();
        }
}    

void FSM10store() {
    clearBitInOutput(engine_Right | light_Yellow);
    if (sens_change(entry) == 2) 
    {
        state = 3;
        InputTime();
        clearBitInOutput(LED_Q1);
    }
    if (sens_change(key_Start) == 1) state = 1;
}

void FSM11exit() {
    clearBitInOutput(engine_Right | switch_Off | LED_Q2 | light_Yellow);
    setBitInOutput(light_Green | LED_Start);
    if (sens_change(entry) == 2) 
    {   
        state = 3;
        InputTime();
        clearBitInOutput(LED_Q1);
    }
    if (sens_change(key_Start) == 1) state = 1;
}

void InputTime() {   
    time(&daten.inputT);
    //printf("Input Time: %ld\n", daten.inputT);
    tmnow = localtime(&daten.inputT);
    printf("Input Time:%2d.%2d.%2d\n",tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
}

void OutputTime() {
    time(&daten.outputT);
    //printf("Output Time: %ld\n\n", daten.outputT);
    tmnow = localtime(&daten.outputT);
    printf("Output Time:%2d.%2d.%2d\n\n",tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
}

