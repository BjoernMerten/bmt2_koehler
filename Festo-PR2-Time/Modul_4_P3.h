
    
struct WKDatenList{
    int lfd;
    time_t inputT;
    char height[16];
    char metal[29];
    time_t outputT;
    struct WKDatenList *next;   
    };
    

#ifndef MODUL_4_P3_H
#define	MODUL_4_P3_H

#ifdef	__cplusplus
extern "C" {
#endif

// functions for practical training 3 - File: Modul_4_P3.c
    
    void writeList();
    void listParts();
    void loadParts();
    void saveParts();
    void deleteParts();
    void cleanUp();


#ifdef	__cplusplus
}
#endif

#endif	/* MODUL_4_P3_H */

