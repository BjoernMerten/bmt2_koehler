/* 
 * File:   Modul_Basic.h
 * Author: Björn Merten & Daniel Friedrich
 *
 * Created on 31. März 2014, 16:35
 */

#ifndef MODUL_1_BASIC_H
#define	MODUL_1_BASIC_H

#define engine_Right 0x0001
#define engine_Left 0x0002
#define engine_Slow 0x0004
#define engine_Stop 0x0008
#define switch_Off 0x0010
#define light_Green 0x0020
#define light_Yellow 0x0040
#define light_Red 0x0080

#define entry 0x0001
#define inHeightcheck 0x0002
#define heigh_Ok 0x0004
#define piece_In_Switch 0x0008
#define metal_Ok 0x0010
#define switch_Open 0x0020
#define store_Full 0x0040
#define exit 0x0080

#define LED_Start 0x0100
#define LED_Reset 0x0200
#define LED_Q1 0x0400
#define LED_Q2 0x0800

#define key_Start 0x0100
#define key_Stop 0x0200
#define key_Reset 0x0400
#define e_Stop 0x0800

#ifdef	__cplusplus
extern "C" {
#endif

// basic funcions - for all tasks - File: Modul_1_Basic.c
     
    void Initialisierung();
    void updateProcessImage();
    void applyOutputToProcess();
    unsigned int isBitSet(unsigned short ChMask);
    void setBitInOutput(unsigned short SetMask);
    void clearBitInOutput(unsigned short ClearMask);
    void resetOutputs();
    void clearInput();
    
    int sens_change(unsigned short Mask);
    
    void InputTime();
    void OutputTime();
    
#ifdef	__cplusplus
}
#endif

#endif	/* MODUL_BASIC_H */