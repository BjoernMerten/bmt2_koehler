#ifndef MODUL_2_P1_H
#define	MODUL_2_P1_H

#ifdef	__cplusplus
extern "C" {
#endif

// functions for practical training 1 - File: Modul_2_P1    
    
    void Testrun();
    void Sensorstatus();
    void stop();
   
    void entryPrint(int c);
    void inHeightcheckPrint(int c);
    void heigh_OkPrint(int c);
    void piece_In_SwitchPrint(int c);
    void metal_OkPrint(int c);
    void switch_OpenPrint(int c);
    void store_FullPrint(int c);
    void exitPrint(int c);
    void key_StartPrint(int c);
    void key_StopPrint(int c);
    void key_ResetPrint(int c);
    void e_StopPrint(int c);
    
    int rnd();


#ifdef	__cplusplus
}
#endif

#endif	/* MODUL_2_P1_H */

