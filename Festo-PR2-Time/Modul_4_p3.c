#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <string.h>
#include "cbw.h"
#include "Modul_1_Basic.h"
#include "Modul_2_P1.h"
#include "Modul_3_P2.h"
#include "Modul_4_P3.h"

char *filename="part.csv";
int ref=1;
struct WKDatenList *firstpart;

extern struct WKDaten daten;

void writeList(){
    
    struct WKDatenList *newpart;
    struct WKDatenList *currentpart;
    
    newpart=malloc(sizeof(struct WKDatenList));
    
    if (firstpart==NULL)
        currentpart=firstpart=newpart;
    
    else
    {
        currentpart=firstpart;
        
        while (currentpart->next != NULL)
            currentpart=currentpart->next;
        
        currentpart->next=newpart;
        currentpart=newpart;
    }
    
    currentpart->lfd=ref;
    currentpart->inputT=daten.inputT;
    strcpy(currentpart->height,daten.height);
    strcpy(currentpart->metal,daten.metal);
    currentpart->outputT=daten.outputT;
    
    ref++;
    
    currentpart->next=NULL; 
}

void listParts(){
    
    struct WKDatenList *currentpart;
    
    if (firstpart==NULL)
        printf("Keine Werkstueckdaten vorhanden.");
    else
    {
        currentpart=firstpart;
        printf("Lfd. Nr\tBeginn\t\tHoehe\t\tmetallisch\tEnde\n\n");
        do
        {
            printf("%i\t", currentpart->lfd);
            tmnow = localtime(&currentpart->inputT);
            printf("%2d.%2d.%2d\t",tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
            printf("%s\t\t", currentpart->height);
            printf("%s\t\t", currentpart->metal);
            tmnow = localtime(&currentpart->outputT);
            printf("%2d.%2d.%2d\n",tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
                    
        }
        while((currentpart=currentpart->next) != NULL);
    }
}

void loadParts(){

FILE *datafile;
struct WKDatenList *newpart;
struct WKDatenList *currentpart;
firstpart = NULL;

char buffer[255];
int i=0;
char *ptr;
char ab[5][40];

datafile=fopen(filename, "rt");
if (datafile)
{
    
    firstpart=malloc(sizeof(struct WKDatenList));
    currentpart = firstpart;
        while(1)
        { 
            newpart=malloc(sizeof(struct WKDatenList));
            fgets(buffer, 256, datafile);
            ptr = strtok(buffer,";");
            
            while(ptr != NULL) {
            strcpy(ab[i],ptr);
            i++;
            ptr = strtok(NULL, ";");
           }
        
        
        
        
    }
}
}


/*
void loadParts(){
    
    FILE *datafile;
    struct WKDatenList *newpart;
    struct WKDatenList *currentpart;
    firstpart = NULL;
    
    datafile=fopen(filename, "r");
    if (datafile)
    {
        firstpart=malloc(sizeof(struct WKDatenList));
        currentpart=firstpart;
        while(1)
        {
        newpart=malloc(sizeof(struct WKDatenList));
        fread(currentpart, sizeof(struct WKDatenList), 1, datafile);
        if (currentpart->next==NULL)
            break;
        currentpart->next=newpart;
        currentpart=newpart;
        }
    fclose(datafile);
    ref=currentpart->lfd;
    ref++;
    }
}
 */

void saveParts(){
    
    FILE *datafile;
    struct WKDatenList *currentpart=firstpart;
    
    char Buffer[255];
    char delimeter[] = ";";
    char line_end[] = {'\n', '\0'};
    char lfd_buffer[]= "       ";
    char beginn_buffer[] = "                    ";
    char end_buffer[] = "                    ";
    
    if (currentpart==NULL)
        return;
    
    datafile=fopen(filename, "wt");
    
    while (currentpart != NULL)
    {
        Buffer[0]='\0';
        sprintf(lfd_buffer, "%i", currentpart->lfd);
        strcat(Buffer, lfd_buffer);
        strcat(Buffer, delimeter);
        tmnow = localtime(&currentpart->inputT);
        sprintf(beginn_buffer, "%2d.%2d.%2d", tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
        //sprintf(beginn_buffer, "%i", currentpart->inputT);
        strcat(Buffer, beginn_buffer);
        strcat(Buffer, delimeter);
        strcat(Buffer, currentpart->height);
        strcat(Buffer, delimeter);
        strcat(Buffer, currentpart->metal);
        strcat(Buffer, delimeter);
        tmnow = localtime(&currentpart->outputT);
        sprintf(end_buffer, "%2d.%2d.%2d", tmnow->tm_hour, tmnow->tm_min, tmnow->tm_sec);
        //sprintf(end_buffer, "%i", currentpart->outputT);
        strcat(Buffer, end_buffer);
        strcat(Buffer, line_end);
        fputs(Buffer, datafile);
        Buffer[0]='\0';
        currentpart = currentpart->next;
        fflush(datafile);
    }
    fclose(datafile);
}

void deleteParts(){
    
    int pos;
    struct WKDatenList *previouspart;
    struct WKDatenList *currentpart;
    
    if (firstpart==NULL)
    {
        printf("Keine Werkstueckdaten vorhanden.\n\n"
               "\tBitte beliebige Taste druecken, um fortzufahren.\n\n\t");
        stop();
        return;
    }
    
    listParts();
    printf("\n\n\tZu loeschendes Werkstueck eingeben: ");
    scanf("%d", &pos);
    
    currentpart=firstpart;
    while(currentpart!=NULL)
    {
        if (currentpart->lfd==pos)
        {
            if (currentpart == firstpart)
                firstpart=currentpart->next;
            else
                previouspart->next=currentpart->next;
            free(currentpart);
            printf("\n\n\tWerkstueck %d aus der Liste geloescht!\n", pos);
            printf("\n\tBitte beliebige Taste drucken, um fortzufahren.\n\n\t");
            stop();
            return;
        }
        else
        {
            previouspart=currentpart;
            currentpart=currentpart->next;
        }
    }
    printf("\n\tWerkstueck %d nicht gefunden, keine Loeschung.\n", pos);
    printf("\n\tBitte beliebige Taste druecken, um fortzufahen.\n\n\t");
    stop();
}

void cleanUp() {
    
    struct WKDatenList *currentpart;
    struct WKDatenList *removepart;
    
    currentpart=firstpart;
    
    while(currentpart)
    {
        removepart=currentpart;
        currentpart=currentpart->next;
        free(removepart);
    }
    free(firstpart);
}






