/* 
 * File:   Modul_3_P3.h
 * Author: Björn Merten
 *
 * Created on 7. Oktober 2014, 13:11
 */

struct WKDaten {
    time_t inputT;
    char height[16];
    char metal[29];
    time_t outputT;   
    }daten;

struct tm *tmnow;  

#ifndef MODUL_3_P2_H
#define	MODUL_3_P2_H

#ifdef	__cplusplus
extern "C" {
#endif

// functions for practical training 2 - File: Modul_3_P2.c

    void FSM();
    void FSM1start();
    void FSM2work();
    void FSM3transportToHeighcheck();
    void FSM4highcheck();
    void FSM5transportToMagCheckPH();
    void FSM6transportToMagCheckNH();
    void FSM7magCheck();
    void FSM8sortNM();
    void FSM9sortPM();
    void FSM10store();
    void FSM11exit();
    void emergency();


#ifdef	__cplusplus
}
#endif

#endif	/* MODUL_3_P2_H */

