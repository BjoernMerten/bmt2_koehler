#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <ctype.h>
#include <conio.h>
#include "cbw.h"
#include "Modul_1_Basic.h"


int main(int argc, char** argv) {
    
    char choose;
    
    Initialisierung();
   
    do
    {
        printf("\n\n\t\tFESTO-Anlagensteuerung\n\n"
               "\n\tBitte waehlen Sie das gewuenschte Programm:\n\n"
               "\tA - Testprogramm FESTO-Laufbandanlage     - Praktikum 1\n\n"
               "\tB - Start der Anlage mit Sortierfunktion  - Praktikum 2\n\n"
               "\tC - Sortierliste anzeigen                 - Praktikum 3\n\n"
               "\tD - Werkstueck aus Sortierliste entfernen - Praktikum 3\n\n"
               "\tE - Programm beenden\n\n");
               
        
        
        choose=getch();
        choose=toupper(choose);
        
        system("cls");
        
        
      
        switch (choose)
        {
            case 'A':
                resetOutputs;
                Testrun();
                break;
            
            case 'B':
                printf("\n\tAnlage laeuft im Betriebmodus Sortierfunktion\n\n");
                resetOutputs;
                //loadParts();
                FSM();
                saveParts();
                cleanUp();
                system("cls");
                break;
         
            case 'C':
                printf("\n\n\tAnzeige Werkstueckliste\n\n\n");
                //loadParts();
                listParts();
                printf("\n\n\tZum Fortfahren bitte beliebige Taste druecken.\n\n\t");
                stop();
                cleanUp();
                system("cls");
                break;
            
            case 'D':
                printf("\n\n\tAnzeige Werkstueckliste\n\n\n");
                //loadParts();
                deleteParts();
                saveParts();
                cleanUp();
                system("cls");
                break;    
        }
       
    } while (choose!='E');
    
    
    
    
    return (EXIT_SUCCESS);
}
