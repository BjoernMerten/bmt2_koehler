#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <time.h>
#include "cbw.h"
#include "Modul_1_Basic.h"
#include "Modul_2_P1.h"


void Testrun() {
    char chose;
    int x=0;
    int z=50;

    for (;;) {
        printf("\n\n\tTestprogramm Festo-Laufbandanlage\n\n\n"
                "\tBitte treffen sie folgende Auswahl:\n\n"
                "\t1\t Laufband laeuft nach links\n\n"
                "\t2\t Laufband laeuft nach rechts\n "
                "\t\t Ampel steht auf rot\n\n"
                "\t3\t Laufband laeuft nach rechts\n "
                "\t\t Weiche ist auf\n"
                "\t\t Ampel steht auf gelb\n\n"
                "\t4\t Aktoren werden zurueckgesetzt\n\n"
                "\t5\t Q2-Leuchte und Start-Taste leuchten\n\n"
                "\t6\t Sensordaten auslesen\n\n"
                "\t7\t Disco\n\n"
                "\t\t Andere Eingaben beenden das Programm\n\n"
                );

        chose=getch();
        fflush(stdin);

        

        if (chose < 48) {
            break;
        }

        if (chose > 55) {
            break;
        }

        system("cls");

        switch (chose) {
            case '1':
                setBitInOutput(engine_Left);
                applyOutputToProcess();
                printf("\n\tLaufband laeuft nach links\n"
                        "\n\tBitte Enter-Taste druecken, um das Laufband zu stoppen"
                        "\n\tund um ins Hauptmenue zurueckzukehren");
                stop();
                clearBitInOutput(engine_Left);
                applyOutputToProcess();
                system("cls");
                break;

            case '2':
                setBitInOutput(engine_Right | light_Red);
                applyOutputToProcess();
                printf("\n\tLaufband laueft nach rechts\n"
                        "\n\tRote Lampe leuchtet\n"
                        "\n\tBitte Enter-Taste druecken, um ins Hauptmenue zurueckzukehren\n");
                stop();
                system("cls");
                break;

            case '3':
                setBitInOutput(engine_Right | switch_Off | light_Yellow);
                applyOutputToProcess();
                printf("\n\tLaufband laueft nach rechts\n"
                        "\n\tWeiche ist offen\n"
                        "\n\tGelbe Lampe leuchtet\n"
                        "\n\tBitte Enter-Taste druecken, um ins Hauptmenue zurueckzukehren\n");
                stop();
                system("cls");
                break;

            case '4':
                resetOutputs();
                printf("\n\tAlle Aktoren gestoppt\n"
                        "\n\tBitte Enter-Taste druecken, um ins Hauptmenue zurueckzukehren\n");
                stop();
                system("cls");
                break;

            case '5':
                setBitInOutput(LED_Q2 | LED_Start);
                applyOutputToProcess();
                printf("\n\tLED Q2 leuchtet\n"
                        "\n\tStarttasten LED leuchtet\n"
                        "\n\tBitte Enter-Taste druecken, um ins Hauptmenue zurueckzukehren\n");
                stop();
                system("cls");
                break;

            case '6':
                Sensorstatus();
                break;
                
            case '7':
                while(1)
                {
                    updateProcessImage();
                    x=rnd();
                    switch(x){
                            case 1:
                                setBitInOutput(light_Green);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Green);
                                break;
                            case 2:
                                setBitInOutput(light_Red);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Red);
                                break;
                            case 3:
                                setBitInOutput(light_Yellow);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Yellow);
                                break;  
                            case 4:
                                setBitInOutput(light_Green | light_Red | switch_Off);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Green | light_Red | switch_Off);
                                break;
                            case 5:
                                setBitInOutput(light_Green | light_Yellow);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Green | light_Yellow);
                                break;    
                            case 6:
                                setBitInOutput(light_Red | light_Yellow);
                                applyOutputToProcess();
                                Sleep(z);
                                clearBitInOutput(light_Red | light_Yellow);
                                break;   
                    }
                    
                    if (sens_change(e_Stop)==2)
                        break;
                }
                
                break;
                
                

        }
        system("cls");
    }
    fflush(stdin);
    system("cls");
}


void Sensorstatus() {
    updateProcessImage();


    entryPrint(isBitSet(entry));
    inHeightcheckPrint(isBitSet(inHeightcheck));
    heigh_OkPrint(isBitSet(heigh_Ok));
    piece_In_SwitchPrint(isBitSet(piece_In_Switch));
    metal_OkPrint(isBitSet(metal_Ok));
    switch_OpenPrint(isBitSet(switch_Open));
    store_FullPrint(isBitSet(store_Full));
    exitPrint(isBitSet(exit));
    key_StartPrint(isBitSet(key_Start));
    key_StopPrint(isBitSet(key_Stop));
    key_ResetPrint(isBitSet(key_Reset));
    e_StopPrint(isBitSet(e_Stop));

    printf("\n\n\n\tBitte Enter-Taste druecken, um ins Hauptmenue zurueckzukehren\n");

    stop();

    /*while (1) {
        updateProcessImage();
    }*/

    system("cls");
}

void stop() {
    for (;;) {
        if (_kbhit())
        {
            getch();
            break;
        }
    }
}

void entryPrint(int c) {
    if (c == 1) {
        printf("\tKein Werkstueck im Einlauf\n\n");
    } else
        printf("\tWerkstueck im Einlauf\n\n");
}

void inHeightcheckPrint(int c) {
    if (c == 1) {
        printf("\tKein Werkstueck in Hoehenmessung\n\n");
    } else
        printf("\tWerkstueck in Hoehenmessung\n\n");
}

void heigh_OkPrint(int c) {
    if (c == 1) {
        printf("\tWerkstueckhoehe im Toleranzbereich\n\n");
    } else
        printf("\tWerkstueckhoehe ausserhalb Toleranzbereich\n\n");
}

void piece_In_SwitchPrint(int c) {
    if (c == 1) {
        printf("\tKein Werkstueck in Weiche\n\n");
    } else
        printf("\tWerkstueck in Weiche\n\n");
}

void metal_OkPrint(int c) {
    if (c == 1) {
        printf("\tWerkstueck aus Metall\n\n");
    } else
        printf("\tWerkstueck nicht aus Metall\n\n");
}

void switch_OpenPrint(int c) {
    if (c == 1) {
        printf("\tWeiche ist offen\n\n");
    } else
        printf("\tWeiche ist geschlossen\n\n");
}

void store_FullPrint(int c) {
    if (c == 1) {
        printf("\tRutsche ist nicht voll\n\n");
    } else
        printf("\tRutsche ist voll\n\n");
}

void exitPrint(int c) {
    if (c == 1) {
        printf("\tKein Werkstueck im Auslauf\n\n");
    } else
        printf("\tWerkstueck im Auslauf\n\n");
}

void key_StartPrint(int c) {
    if (c == 1) {
        printf("\tStarttaste ist gedrueckt\n\n");
    } else
        printf("\tStarttaste ist nicht gedrueckt\n\n");
}

void key_StopPrint(int c) {
    if (c == 1) {
        printf("\tStoptaste ist nicht gedrueckt\n\n");
    } else
        printf("\tStoptaste ist gedrueckt\n\n");
}

void key_ResetPrint(int c) {
    if (c == 1) {
        printf("\tResettaste ist gedrueckt\n\n");
    } else
        printf("\tResettaste ist nicht gedrueckt\n\n");
}

void e_StopPrint(int c) {
    if (c == 1) {
        printf("\tNotstop ist nicht gedrueckt\n\n");
    } else
        printf("\tNotstop ist gedrueckt\n\n");
}

int rnd()
{
    int r;
    
    r=rand()%6+1;
    return r;
}


