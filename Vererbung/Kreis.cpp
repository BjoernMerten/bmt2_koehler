#include "Kreis.h"

Kreis::Kreis(char *n, float a) : Flaeche(n) {
    radius=a;
}

float Kreis::berechneFlaeche() const {
    return radius*radius*3.141593F;
}

Kreis::~Kreis(){
}

