#include <cstdlib>
#include <iostream>
using namespace std;
#include "Kreis.h"
#include "Ring.h"


int main(int argc, char** argv) {
    Kreis blau_kreis("Blau",5);
    Ring gelb_ring("Gelb",10,3);
    blau_kreis.getFarbe();
    cout << "\n" << blau_kreis.berechneFlaeche();
    gelb_ring.getFarbe();
    cout << "\nRingflaeche: " << gelb_ring.berechneFlaeche()
         << "\nFlaeche des Grundkreises: " << gelb_ring.Kreis::berechneFlaeche()
         << "\n";
    
    return 0;
}

