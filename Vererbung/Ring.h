#ifndef RING_H
#define	RING_H
#include "Kreis.h"

class Ring : public Kreis{
    public:
        Ring(char *n, float aussen, float innen);
        ~Ring();
        float berechneFlaeche() const;
        private:
            float innenradius;
};
#endif	/* RING_H */

