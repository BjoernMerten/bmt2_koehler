#include "Ring.h"

Ring::Ring(char* n, float aussen, float innen) : Kreis(n, aussen){
    innenradius = innen;
}

Ring::~Ring(){
}

float Ring::berechneFlaeche() const{
    return (Kreis::berechneFlaeche()-
            (innenradius*innenradius*3.141593F));
}