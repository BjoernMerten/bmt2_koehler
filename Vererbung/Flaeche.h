#ifndef FLAECHE_H
#define	FLAECHE_H

class Flaeche {
    public:
        Flaeche(char *n);
        ~Flaeche();
        void getFarbe() const;
    private:
        char farbe[11];
    };
    
#endif	/* FLAECHE_H */   