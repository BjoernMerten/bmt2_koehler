#ifndef KREIS_H
#define	KREIS_H
#include "Flaeche.h"


    class Kreis : public Flaeche {
    public:
        Kreis(char *n, float a);
        float berechneFlaeche() const;
        ~Kreis();
    private: 
        float radius;
        
    };


#endif	/* KREIS_H */

