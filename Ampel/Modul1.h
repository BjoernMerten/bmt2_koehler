/* 
 * File:   Modul1.h
 * Author: Björn Merten
 *
 * Created on 14. Oktober 2014, 09:01
 */

#ifndef MODUL1_H
#define	MODUL1_H

#ifdef	__cplusplus
extern "C" {
#endif

    
    void FSM();
    void FSMRB();
    void FSMBB();
    
    int eins();
    int zwei();
    int drei();
    int vier();
    int fuenf();
    int sechs();
    int sieben();
    int acht();
    
    int N();
    int Y();
    
    



#ifdef	__cplusplus
}
#endif

#endif	/* MODUL1_H */

