#include <cstdlib>
#include <iostream>

#include "Stack.h"

using namespace std;

void test_stack(int n){
    
    MeinStack s(n);
    for (int i=0; i<n; i++)
        s.push(i); //alternativ Zufallswerte für i
    s.info_stack();
    
    while (!s.empty())
        cout << dec << s.pop() << endl;
    
}




int main() {

    int anzahl = 0;
    cout << "Bitte Anzahl eingeben";
    cin >> anzahl;
    
    test_stack(anzahl);
    
    return 0;
}

