#ifndef STACK_H
#define	STACK_H

#include <iostream>

typedef int T;

void test_stack(int);




class MeinStack{
private:
    int Sptr;
    T *K;
    int Max;

public:
    MeinStack(int s);
    ~MeinStack();
    
    
    void push(T a);
    T pop();
    bool full();
    bool empty();
    void info_stack();
    
};









#endif	/* STACK_H */

